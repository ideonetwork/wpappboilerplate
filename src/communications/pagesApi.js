import axios from 'axios'
import { apiUrl } from '../config'

/** @function public getPostsRequest(page).

This function return the request of the list of posts.

@param {integer} page the page of posts that the request should ask.
@param {integer} categoryId the id of the category.
@param {integer} searchText the text to search.
*/

export function getPageRequest (pageId) {
  // return request
  return axios.get(`${apiUrl}/pages/${pageId}`)
}
