import {
  Dimensions,
  Platform
} from 'react-native'

import theme from '../../template/baseTheme'

const navigationBarHeight = Platform.OS === 'ios' ? 64 : 80

const screenHeight = Dimensions.get('window').height - navigationBarHeight
const screenWidth = Dimensions.get('window').width

const styles = {
  content: {
    backgroundColor: '#fff',
    width: screenWidth,
    height: screenHeight
  },
  postsList: {

  },
  postsListItem: {

  },
  postsListItemTitle: {

  },
  postsListItemSubtitle: {
    color: '#999',
    fontSize: 12
  }
}

export default styles
