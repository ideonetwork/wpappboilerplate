import {
  Dimensions,
  Platform
} from 'react-native'

import theme from '../../template/baseTheme'

const navigationBarHeight = Platform.OS === 'ios' ? 64 : 80

const screenHeight = Dimensions.get('window').height - navigationBarHeight
const screenWidth = Dimensions.get('window').width

const styles = {
  content: {
    backgroundColor: '#fff',
    width: screenWidth,
    height: screenHeight
  },
  postContainer: {

  },
  postDataContainer: {
    padding: 15
  },
  postTitleContainer: {
    marginBottom: 15,
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: theme.brandPrimary
  },
  postTitle: {
    textAlign: 'left',
    fontSize: 22
  },
  postTitleIcon: {
    fontSize: 20
  },
  postImage: {
    width: screenWidth,
    height: 200,
    top: -15,
    left: -15,
    marginBottom: 0
  },
  postCommentsContainer: {
    padding: 15
  },
  postCommentsTitleContainer: {
    marginBottom: 15,
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: theme.brandPrimary
  },
  postCommentsTitle: {
    textAlign: 'left',
    fontSize: 20
  },
  postCommentsTitleIcon: {
    fontSize: 18
  },
  postCommentsList: {

  },
  postComment: {

  },
  postCommentTitle: {

  },
  postCommentSubtitle: {
    color: '#999',
    fontSize: 12
  },
  postCommentsMoreButton: {
    marginTop: 15
  },
  postNewCommentContainer: {
    padding: 15,
    marginBottom: 15
  },
  postNewCommentText: {
    color: '#999',
    fontSize: 12
  },
  postNewCommentButton: {
    marginTop: 15,
    alignSelf: 'flex-end'
  },
  postNewCommentInput: {
    color: '#999',
    fontSize: 12,
    height: 50
  }
}

export default styles
