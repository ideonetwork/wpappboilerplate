import {
  Dimensions,
  Platform
} from 'react-native'

import theme from '../../template/baseTheme'

const navigationBarHeight = Platform.OS === 'ios' ? 64 : 80

const screenHeight = Dimensions.get('window').height - navigationBarHeight
const screenWidth = Dimensions.get('window').width

const styles = {
  content: {
    backgroundColor: '#fff',
    width: screenWidth,
    height: screenHeight
  },
  profileContainer: {
    padding: 15
  },
  profileActions: {
    marginTop: 30
  },
  profileAction: {
    marginBottom: 15
  },
  profileData: {

  },
  profileDataTitle: {
    fontSize: 22
  },
  profileDataIcon: {
    fontSize: 24,
    color: theme.brandPrimary
  },
  formInputGroup: {
    marginBottom: 15
  },
  formIcon: {
    color: theme.brandPrimary
  }
}

export default styles
