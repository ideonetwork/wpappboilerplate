import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
// import { bindActionCreators } from 'redux'

import { View, Linking, Text, Image } from 'react-native'
import { Container, Content, Spinner } from 'native-base'
import HTMLView from 'react-native-htmlview'
import theme from '../../template/baseTheme'
import styles from './styles'

import { serializeWpPage } from '../../serializers/pagesSerializers'
import { getPageRequest } from '../../communications/pagesApi'
import { RequiredDataException } from '../../exceptions/systemExceptions'

class Page extends Component {

  constructor (props) {
    super(props)

    // set initial state
    this.state = {
      isLoad: false,
      page: null
    }

    // check post object is present
    if (!this.props.route.id) {
      throw new RequiredDataException('Page.constructor() : the Page component require a post object received from router')
    }
  }

  componentDidMount () {
    this.loadPage()
  }

  /** @function public loadPage().

  This function load the requested page, serialize it and set it as state.
  */

  loadPage () {
    this.setState({isLoad: true})
    getPageRequest(this.props.route.id)
    .then((response) => {
      console.log('getPageRequest', response)
      let page = serializeWpPage(response.data)
      console.log('page', page)
      this.setState({
        isLoad: false,
        page
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  /** @function renderPageHeader().

  This function render the page header to the user.
  */

  renderPageHeader () {
    if (this.state.page.thumbnailLarge) {
      return (
        <Image style={styles.pageImage} source={{uri: this.state.page.thumbnailLarge}} blurRadius={2}>
          <View style={styles.pageImageTitleContainer}>
            <Text style={styles.pageImageTitle}>{this.state.page.title.toUpperCase()}</Text>
          </View>
        </Image>
      )
    } else {
      return (
        <View style={styles.pageTitleContainer}>
          <Text style={styles.pageTitle}>{this.state.page.title}</Text>
        </View>
      )
    }
  }

  /** @function renderPage().

  This function render the page content to the user (if it exist).
  */

  renderPage () {
    if (this.state.page) {
      return (
        <View style={styles.pageContainer}>
          {this.renderPageHeader()}
          <Text>
            <HTMLView
              value={this.state.page.content}
              onLinkPress={(url) => Linking.openURL(url)}
            />
          </Text>
        </View>
      )
    } else {
      return null
    }
  }

  render () {
    return (
      <Container theme={theme}>
        <View style={styles.content}>
          <Content>
            {this.state.isLoad ? <Spinner style={{alignSelf: 'center'}} color='#333' /> : this.renderPage()}
          </Content>
        </View>
      </Container>
    )
  }

}

Page.propTypes = {
  route: PropTypes.object,
  navigator: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page)
