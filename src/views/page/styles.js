import {
  Dimensions,
  Platform
} from 'react-native'

import theme from '../../template/baseTheme'

const navigationBarHeight = Platform.OS === 'ios' ? 64 : 80

const screenHeight = Dimensions.get('window').height - navigationBarHeight
const screenWidth = Dimensions.get('window').width

const styles = {
  content: {
    backgroundColor: '#fff',
    width: screenWidth,
    height: screenHeight
  },
  pageContainer: {
    padding: 15
  },
  pageTitleContainer: {
    marginBottom: 15,
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: theme.brandPrimary
  },
  pageTitle: {
    textAlign: 'left',
    fontSize: 28
  },
  pageImage: {
    width: screenWidth,
    height: 200,
    marginBottom: 0,
    left: -15,
    top: -15
  },
  pageImageTitleContainer: {
    width: screenWidth,
    height: 200,
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    justifyContent: 'flex-end',
    padding: 15
  },
  pageImageTitle: {
    color: 'rgba(255,255,255,0.9)',
    fontSize: 28,
    width: screenWidth - 30
  }
}

export default styles
