import {
  Dimensions,
  Platform
} from 'react-native'

import theme from '../../template/baseTheme'

const navigationBarHeight = Platform.OS === 'ios' ? 0 : 24

const screenHeight = Dimensions.get('window').height - navigationBarHeight
const screenWidth = Dimensions.get('window').width

const menuWidth = screenWidth / 1.5
const logoHeight = 120

const styles = {
  menuContainer: {
    width: menuWidth,
    height: screenHeight,
    backgroundColor: theme.brandPrimary,
    paddingTop: 20
  },
  logoContainer: {
    width: menuWidth,
    height: logoHeight
  },
  logoBackground: {
    width: menuWidth,
    height: logoHeight
  },
  logo: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: menuWidth,
    height: logoHeight,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoImage: {
    height: logoHeight / 1.5, // -> Update here size for logo
    width: logoHeight / 1.5
  },
  categoriesContainer: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 10,
    height: screenHeight - logoHeight - 85
  },
  category: {
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#fff'
  },
  categoryTitle: {
    color: '#fff',
    fontSize: 20
  },
  socialContainer: {
    width: menuWidth,
    marginBottom: 30
  },
  socialIconContainer: {
    alignSelf: 'center'
  },
  settingsPagesContainer: {
    width: menuWidth
  },
  settingsPageButton: {
    alignSelf: 'center'
  },
  settingsPageText: {
    color: '#fff',
    fontSize: 10
  },
  settingsPageIcon: {
    color: '#fff',
    fontSize: 10
  }
}

export default styles
