import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as postsActions from '../../actions/postsActions'
import * as stateActions from '../../actions/stateActions'

import { View, ScrollView, Text, TouchableOpacity, Image, Linking } from 'react-native'
import { Button, Icon, Grid, Col } from 'native-base'
import Posts from '../../views/posts'
import Page from '../../views/page'
import styles from './styles'
import { socialUrls, settingsPages } from '../../config'

import logo from '../../assets/brand/logo.png'
import background from '../../assets/brand/background.jpg'

class Menu extends Component {

  constructor (props) {
    super(props)

    this.socials = this.filterSocials()
  }

  /** @function public updateCategory(category).

  This function update the selected category from user and load the View
  with all posts of this category.

  @param {object} category the category object to show to user.
  */

  updateCategory (category) {
    this.props.postsActions.setCurrentPage(1)
    this.props.postsActions.setCurrentCategory(category)
    this.props.stateActions.setSearchText('')

    this.props.navigateTo({
      component: Posts
    })
  }

  /** @function public goToPage(pageId).

  This function open a specific page with page view from page id.

  @param {integer} pageId the id of the page to show.
  */

  goToPage (pageId) {
    this.props.navigateTo({
      component: Page,
      id: pageId
    })
  }

  /** @function public openUrl(url).

  This function open a specific url on user browser.

  @param {string} url the to open on browser.
  */

  openUrl (url) {
    Linking.openURL(url)
  }

  /** @function public filterSocials().

  This function filters all social from config file and return a limited number of
  them.
  */

  filterSocials () {
    const limit = 4
    let counter = 0
    let filteredSocials = []
    if (socialUrls.facebook && socialUrls.facebook !== '' && counter < limit) {
      filteredSocials.push({icon: 'logo-facebook', url: socialUrls.facebook})
      counter++
    }
    if (socialUrls.twitter && socialUrls.twitter !== '' && counter < limit) {
      filteredSocials.push({icon: 'logo-twitter', url: socialUrls.twitter})
      counter++
    }
    if (socialUrls.linkedin && socialUrls.linkedin !== '' && counter < limit) {
      filteredSocials.push({icon: 'logo-linkedin', url: socialUrls.linkedin})
      counter++
    }
    if (socialUrls.gplus && socialUrls.gplus !== '' && counter < limit) {
      filteredSocials.push({icon: 'logo-googleplus', url: socialUrls.gplus})
      counter++
    }
    if (socialUrls.youtube && socialUrls.youtube !== '' && counter < limit) {
      filteredSocials.push({icon: 'logo-youtube', url: socialUrls.youtube})
      counter++
    }
    return filteredSocials
  }

  /** @function public renderLogo().

  This function render the blog logo.
  */

  renderLogo () {
    return (
      <View style={styles.logoContainer}>
        <Image source={background} style={styles.logoBackground} />
        <View style={styles.logo}>
          <Image style={styles.logoImage} source={logo} />
        </View>
      </View>
    )
  }

  /** @function public renderCategories().

  This function render the blog categories.
  */

  renderCategories () {
    return (
      <View style={styles.categoriesContainer}>
        <ScrollView>
          <TouchableOpacity style={styles.category} onPress={() => this.updateCategory(null)}>
            <Text style={styles.categoryTitle}>Blog</Text>
          </TouchableOpacity>
          {this.props.categories.map((category, key) => {
            return (
              <TouchableOpacity key={key} style={styles.category} onPress={() => this.updateCategory(category)}>
                <Text style={styles.categoryTitle}> - {category.name}</Text>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }

  /** @function public renderSocial().

  This function render the blog social urls.
  */

  renderSocial () {
    return (
      <View style={styles.socialContainer}>
        <Grid>
          {this.socials.map((social, key) => {
            return (
              <Col key={key}>
                <Button onPress={() => this.openUrl(social.url)} transparent style={styles.socialIconContainer}>
                  <Icon style={{color: '#fff'}} name={social.icon} />
                </Button>
              </Col>
            )
          })}
        </Grid>
      </View>
    )
  }

  /** @function public renderStiingsPages().

  This function render the blog settings pages buttons.
  */

  renderSettingsPages () {
    if (settingsPages && settingsPages.length) {
      return (
        <View style={styles.settingsPagesContainer}>
          <Grid>
            {settingsPages.map((page, key) => {
              return (
                <Col key={key}>
                  <Button style={styles.settingsPageButton} onPress={() => this.goToPage(page.id)} transparent>
                    <Text style={styles.settingsPageText}><Icon style={styles.settingsPageIcon} name={page.icon} /> {page.name}</Text>
                  </Button>
                </Col>
              )
            })}
          </Grid>
        </View>
      )
    } else {
      return null
    }
  }

  render () {
    return (
      <View style={styles.menuContainer}>
        {this.renderLogo()}
        {this.renderCategories()}
        {this.renderSocial()}
        {this.renderSettingsPages()}
      </View>
    )
  }

}

Menu.propTypes = {
  navigateTo: PropTypes.func,
  categories: PropTypes.array,
  postsActions: PropTypes.object,
  stateActions: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
  return {
    categories: Object.values(state.postsReducer.categories)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
    stateActions: bindActionCreators(stateActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
