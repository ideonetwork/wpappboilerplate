import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as stateActions from '../../actions/stateActions'
import * as postsActions from '../../actions/postsActions'

import { Header, Title, Button, Icon, Input, InputGroup } from 'native-base'
import Posts from '../../views/posts'
import styles from './styles'
import baseTheme from '../../template/baseTheme'

class NavigationBar extends Component {

  constructor (props) {
    super(props)
  }

  /** @function public startSearch().

  This function init the search request and send user to the posts view.
  */

  startSearch () {
    this.props.postsActions.setCurrentPage(1)
    this.props.postsActions.setCurrentCategory(null)
    this.props.stateActions.setSearchStatus(false)

    this.props.navigator.resetTo({
      component: Posts
    })
  }

  /** @function public renderNormalBar().

  This function render the navigationBar as a normal bar with title and menu
  button.
  */

  renderNormalBar () {
    return (
      <Header style={styles.navigationBar}>
        <Button onPress={() => this.props.stateActions.setDrawerStatus(true)} transparent>
          <Icon name='md-menu' style={styles.navigationBarIcon} />
        </Button>
        <Title style={styles.title}>{this.props.title}</Title>
        <Button onPress={() => this.props.stateActions.setSearchStatus(true)} transparent>
          <Icon name='md-search' style={styles.navigationBarIcon} />
        </Button>
      </Header>
    )
  }

  /** @function public renderSearchBar().

  This function render the navigationBar as a search bar with a input field.
  */

  renderSearchBar () {
    return (
      <Header style={styles.navigationBar} theme={baseTheme} searchBar rounded>
        <InputGroup style={styles.searchInputGroup}>
          <Icon name='md-search' />
          <Input placeholder='Cerca' value={this.props.searchText}
            onSubmitEditing={() => this.startSearch()}
            onChangeText={(text) => this.props.stateActions.setSearchText(text)} />
        </InputGroup>
        <Button onPress={() => this.props.stateActions.setSearchStatus(false)} transparent>
          <Icon style={styles.searchCloseIcon} name='md-close' />
        </Button>
      </Header>
    )
  }

  render () {
    if (this.props.searchStatus) {
      return this.renderSearchBar()
    } else {
      return this.renderNormalBar()
    }
  }

}

NavigationBar.propTypes = {
  title: PropTypes.string,
  searchStatus: PropTypes.bool,
  stateActions: PropTypes.object,
  navigator: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
  return {
    title: state.stateReducer.viewTitle,
    searchStatus: state.stateReducer.searchStatus,
    searchText: state.stateReducer.searchText
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    stateActions: bindActionCreators(stateActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar)
