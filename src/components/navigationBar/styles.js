import theme from '../../template/baseTheme'

const styles = {
  navigationBar: {
    backgroundColor: theme.brandPrimary
  },
  title: {
    color: theme.inverseTextColor
  },
  navigationBarIcon: {
    color: '#fff'
  },
  searchCloseIcon: {
    color: '#fff'
  },
  searchInputGroup: {
    backgroundColor: '#fff'
  }
}

export default styles
