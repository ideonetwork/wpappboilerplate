import { convertHTML } from '../helpers/stringsHelpers'

/** @function public serializeWpPage(page).

This function transforms a page object from wordpress API to a serialized page
new object.

@param {object} page the page received from Wordpress.
*/

export function serializeWpPage (page) {
  // initialize new comment object
  let serializedPage = {}
  // generate new comment object
  serializedPage.id = page.id
  serializedPage.date = page.date
  serializedPage.title = convertHTML(page.title.rendered)
  serializedPage.content = page.content.rendered
  serializedPage.thumbnailSmall = (page.better_featured_image ? (
    page.better_featured_image.media_details.sizes.thumbnail.source_url
  ) : null)
  serializedPage.thumbnailLarge = (page.better_featured_image ? (
    page.better_featured_image.source_url
  ) : null)
  // return new page object
  return serializedPage
}
