/** @function public RequiredDataException(message).

This function generate an exception because some required data are not served.

@param {string} message the message of the error.
*/

export function RequiredDataException (message) {
  this.message = message
  this.name = 'RequiredDataException'
}
