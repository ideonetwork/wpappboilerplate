export const SET_STATE_VIEW_TITLE = 'SET_STATE_VIEW_TITLE'
export const SET_STATE_DRAWER_STATUS = 'SET_STATE_DRAWER_STATUS'
export const SET_STATE_SEARCH_STATUS = 'SET_STATE_SEARCH_STATUS'
export const SET_STATE_SEARCH_TEXT = 'SET_STATE_SEARCH_TEXT'

/** @function public setViewTitle(title).

This function set the current view title on the store.

@param {string} title the title of the current view.
*/

export function setViewTitle (title) {
  return {
    type: SET_STATE_VIEW_TITLE,
    title
  }
}

/** @function public setDrawerStatus(status).

This function set the new drawer status on the store.

@param {boolean} status the new boolean status of the drawer.
*/

export function setDrawerStatus (status) {
  return {
    type: SET_STATE_DRAWER_STATUS,
    status
  }
}

/** @function public setSearchStatus(status).

This function set the new searchbar status on the store.

@param {boolean} status the new boolean status of the searchbar.
*/

export function setSearchStatus (status) {
  return {
    type: SET_STATE_SEARCH_STATUS,
    status
  }
}

/** @function public setSearchText(status).

This function set the new searchbar text on the store.

@param {string} text the new text of the searchbar.
*/

export function setSearchText (text) {
  return {
    type: SET_STATE_SEARCH_TEXT,
    text
  }
}
