import { AppRegistry } from 'react-native'
import Root from './src/root'

console.disableYellowBox = true

AppRegistry.registerComponent('WpAppBoilerplate', () => Root)
