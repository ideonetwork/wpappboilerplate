# WP App Boilerplate

Read Wordpress posts from a react-native application.

## Wordpress dependencies

- WP REST API (v 2.0) (https://wordpress.org/plugins/rest-api/)
- Better REST API Featured Images (v 1.2) (https://it.wordpress.org/plugins/better-rest-api-featured-images/)
- JSON Basic Authentication (https://github.com/WP-API/Basic-Auth)

Edit the .htaccess with this data:

```
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{HTTP:Authorization} ^(.*)
RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress
``
